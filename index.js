const database = require('database');

const ID_NOT_FOUND = -1;

const QInsertPerson = '\
  INSERT INTO sp_person (cpf, name) \
  VALUES (?, ?)';

const QUpdatePerson = '\
  UPDATE sp_person \
  SET name = ?, cpf = ? \
  WHERE id = ?';

const QInsertStudent = '\
  INSERT INTO sp_student(username, alu_cod, esp_cod, personid) \
  VALUES (?, ?, ?, ?)';

const QUpdateStudent = '\
  UPDATE sp_student \
    SET username = ? \
  WHERE id = ?';

const QSearchStudent = '\
  SELECT id, personid FROM sp_student \
  WHERE alu_cod = ? AND esp_cod = ?';

const QSearchDiscipline = '\
  SELECT dr.id as disciplineRuleId \
    FROM sp_discipline d \
    INNER JOIN sp_discipline_rules dr ON ( \
      d.id = dr.disciplineid AND \
      d.unit_code = ? AND \
      d.discipline_code = ? AND \
      d.class_code = ? AND \
      dr.semester = ? AND \
      dr.active = 1 \
    )';

const QSearchStudentRules = '\
  SELECT id \
    FROM sp_student_rules \
    WHERE student_id = ? AND disciplineruleid = ? AND semester = ?';

const QInsertStudentRules = '\
  INSERT INTO sp_student_rules (student_id, disciplineruleid, semester) \
    VALUES (?, ?, ?)';


/**
 * Formats data to be used on functions
 */
const formatData = (e) => ({
  cpf: e.cpf,
  aluCod: e.alu_cod,
  espCod: e.esp_cod,
  username: e.username,
  name: e.name
});

/**
 * Formats student data to be used on functions
 */
const formatStudentData = (e, t) => ({
  cpf: e.cpf,
  aluCod: e.alu_cod,
  espCod: e.esp_cod,
  username: e.username,
  name: e.name,
  unitCode: t.unit_id,
  disciplineCode: t.discipline_code,
  classCode: t.class_id,
  semester: t.semester
});

/**
 * Searchs for the disciplineRulesId
 */
const searchDiscipline = (conn, { unitCode, disciplineCode, classCode, semester }) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchDiscipline, [unitCode, disciplineCode, classCode, semester], (err, row) => {
      if (err) { return reject(err); }
      if (!row.length) {
        return resolve(ID_NOT_FOUND);
      }
      return resolve(row[0].disciplineRuleId);
    });
  });
};

/**
 * Searchs for the student rules
 */
const searchStudentRules = (conn, studentId, disciplineRulesId, semester) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchStudentRules, [studentId, disciplineRulesId, semester], (err, row) => {
      if (err) { return reject(err); }
      if (!row.length) {
        return resolve(ID_NOT_FOUND);
      }
      return resolve(row[0].id);
    });
  });
};

/**
 * Inserts a Student Rules on database
 */
const insertStudentRules = (conn, studentId, disciplineRuleId, semester) => {
  return new Promise((resolve, reject) => {
    conn.query(QInsertStudentRules, [studentId, disciplineRuleId, semester], (err, result) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(result.insertId);
    });
  });
};

/**
 * Inserts a Person
 */
const insertPerson = (conn, cpf, name) => {
  return new Promise((resolve, reject) => {
    conn.query(QInsertPerson, [cpf, name], (err, row) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(row.insertId);
    });
  });
};

/**
 * Updates a Person
 */
const updatePerson = (conn, id, name, cpf) => {
  return new Promise((resolve, reject) => {
    conn.query(QUpdatePerson, [name, cpf, id], (err) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(id);
    });
  });
};

/**
 * Searches by a Student on database using his Ra, UnitCode, DisciplineCode and ClassCode
 */
const searchStudent = (conn, aluCod, espCod) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchStudent, [aluCod, espCod], (err, result) => {
      if (err) { return reject(err); }
      if (result.length) { return resolve(result[0]); }
      return resolve(ID_NOT_FOUND);
    });
  });
};

/**
 * Inserts a Student on database
 */
const insertStudent = (conn, username, aluCod, espCod, personId) => {
  return new Promise((resolve, reject) => {
    conn.query(QInsertStudent, [username, aluCod, espCod, personId], (err, result) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(result.insertId);
    });
  });
};

/**
 * Updates a Student on database
 */
const updateStudent = (conn, id, username) => {
  return new Promise((resolve, reject) => {
    conn.query(QUpdateStudent, [username, id], (err) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(id);
    });
  });
};

/**
 * Saves a student on database
 */
const saveStudentOnly = (studentInfo) => {
  return new Promise(async (resolve, reject) => {
    let conn = null;
    let personId = null;
    let studentId = null;
    let student = null;

    try {
      conn = await database.getConnection();
      student = await searchStudent(conn, studentInfo.aluCod, studentInfo.espCod);

      if (student === ID_NOT_FOUND) {
        personId = await insertPerson(conn, studentInfo.cpf, studentInfo.name);
      } else {
        personId = await updatePerson(conn, student.personid, studentInfo.name, studentInfo.cpf);
      }

      if (student === ID_NOT_FOUND) {
        studentId = await insertStudent(conn, studentInfo.username, studentInfo.aluCod, studentInfo.espCod, personId);
      } else {
        studentId = await updateStudent(conn, student.id, studentInfo.username);
      }

      return resolve(studentId);
    } catch (err) {
      return reject(err);
    } finally {
      if (conn) {
        conn.release();
      }
    }
  });
};

/**
 * Saves a student on database
 */
const saveStudent = (studentInfo, enforceDiscipline = true) => {
  return new Promise(async (resolve, reject) => {
    let conn = null;
    let personId = null;
    let student = null;
    let studentId = null;
    let disciplineRuleId = null;
    let studentRuleId = null;

    try {
      conn = await database.getConnection();
      student = await searchStudent(conn, studentInfo.aluCod, studentInfo.espCod);

      if (student === ID_NOT_FOUND) {
        personId = await insertPerson(conn, studentInfo.cpf, studentInfo.name);
      } else {
        personId = await updatePerson(conn, student.personid, studentInfo.name, studentInfo.cpf);
      }

      if (student === ID_NOT_FOUND) {
        studentId = await insertStudent(conn, studentInfo.username, studentInfo.aluCod, studentInfo.espCod, personId);
      } else {
        studentId = await updateStudent(conn, student.id, studentInfo.username);
      }

      disciplineRuleId = await searchDiscipline(conn, studentInfo);

      if (enforceDiscipline && disciplineRuleId === ID_NOT_FOUND) {
        const column = 'unitCode/disciplineCode/classCode/semester';
        const params = `${studentInfo.unitCode}/${studentInfo.disciplineCode}/${studentInfo.classCode}/${studentInfo.semester}`;
        return reject(database.buildEntityNotFoundError('sp_discipline', column, params));
      }

      // Student without gamificated discipline
      if (disciplineRuleId === ID_NOT_FOUND) {
        return resolve(null);
      }

      studentRuleId = await searchStudentRules(conn, studentId, disciplineRuleId, studentInfo.semester);

      if (studentRuleId === ID_NOT_FOUND) {
        studentRuleId = await insertStudentRules(conn, studentId, disciplineRuleId, studentInfo.semester);
      }

      return resolve(studentRuleId);
    } catch (err) {
      return reject(err);
    } finally {
      if (conn) {
        conn.release();
      }
    }
  });
};

module.exports.saveStudent = saveStudent;
module.exports.formatStudentData = formatStudentData;
module.exports.formatData = formatData;
module.exports.saveStudentOnly = saveStudentOnly;
